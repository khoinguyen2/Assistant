import express, { json } from 'express';
import * as dotenv from 'dotenv';
import OpenAI from 'openai';
import { ChatCompletion, ChatCompletionMessageParam } from 'openai/resources/chat';
import { Assistants } from 'openai/resources/beta/assistants/assistants';
import * as fs from 'fs'
import * as path from 'path'

const app = express();
const port = 8080;
dotenv.config();


var openai = new OpenAI({ apiKey: process.env.OPEN_API_KEY });

app.listen(port, () => {
    console.log(`API listening on http://localhost:${port}`);
})

// Middleware to parse JSON
app.use(express.json());

// Middleware to parse URL-encoded data
app.use(express.urlencoded({ extended: true }));
var folderPath: string = 'data/'


async function createOpenAIFilesInFolder(folderPath: string) {
    const openAIFiles = [];
    // Đọc danh sách các file trong thư mục
        const fileNames = fs.readdirSync(folderPath);

        for (const fileName of fileNames) {
            const filePath = path.join(folderPath, fileName);

            // Kiểm tra xem là file thực sự hay không
            if (fs.statSync(filePath).isFile()) {
                // Tạo OpenAI file
                const file = await openai.files.create({
                    file: fs.createReadStream(filePath),
                    purpose: "assistants",

                });
                openAIFiles.push(file.id);
            }
        }
        return openAIFiles;

}


createOpenAIFilesInFolder(folderPath)
  .then(async(files) => {
    // Ở đây, bạn có thể sử dụng danh sách OpenAI files theo nhu cầu của bạn
    console.log("OpenAI files created:", files);

    const assistant = await openai.beta.assistants.create({
        name: "Information Technology Major Roadmap",
        description: "You create Roadmap about learning path in IT ",
        model: "gpt-3.5-turbo",
        tools: [{"type": "code_interpreter"}],
        file_ids: files
      });
  })
  .catch((error) => {
    console.error("Error creating OpenAI files:", error);
  });
